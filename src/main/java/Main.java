import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Main {
//    public static void main(String[] args) {
//        List<Object> myNumberList = new ArrayList<>();
//        myNumberList.add(1L);
//        myNumberList.add(1.0);
//        System.out.println(myNumberList.get(0).getClass());
//        System.out.println(myNumberList.get(1).getClass());
//        Long result = (Long) myNumberList.get(0) + (Long) myNumberList.get(1);
//        System.out.println(result);

//        List<Number> l = new ArrayList<>();
//        l.add(1);
//        dump(l);
//        List<Integer> l1 = new ArrayList<>();
//        l1.add(2);
//        dump(l1);
//    }
//
//    static void dump(Collection<? extends Number> c) {
//        for (Iterator<?> i = c.iterator(); i.hasNext(); ) {
//            Object o = i.next();
//            System.out.println(o);
//        }
//    }

    public static class TestClass {
        public static void print(List<? super String> list) {
            list.add("Hello World!");
            System.out.println(list.get(0));
        }
    }

    public static class TestClass2 {
        public static Number print(List<? extends Number> list2) {
            System.out.println(list2.get(0));
            System.out.println(list2.get(1));
            return list2.get(0);
        }
    }

    public static void main(String []args) {
        List<String> list = new ArrayList<>();
        List<Number> list2= new ArrayList<>();
        list.add("Hello");
        list2.add(2.0);
        list2.add("Two"); // ошибка из-за оргничений при подаче обобщенных параметров в метод
        TestClass.print(list);
        TestClass2.print(list2);
    }

}
